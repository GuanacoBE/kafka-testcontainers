package be.bertaux.test;

import org.junit.jupiter.api.Test;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.junit.jupiter.Testcontainers;

/**
 * Created by jeromebertaux on 19/02/2020.
 */
@Testcontainers
public class ContainerTest {

    public static final KafkaContainer KAFKA_CONTAINER;

    static {
        KAFKA_CONTAINER = new KafkaContainer("5.4.0");
        KAFKA_CONTAINER.start();
    }

    @Test
    void t() {
        System.out.println(KAFKA_CONTAINER.getBootstrapServers());
    }

}
